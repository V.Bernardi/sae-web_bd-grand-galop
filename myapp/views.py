from .app import app, db
from flask import render_template, request, flash, redirect, url_for
from .models import *
# from werkzeug.security import check_password_hash, generate_password_hash

from .models import Eleve, Moniteur


# Page d'accueil sans connexion

@app.route ("/", methods=("get", "post"))
def index():
    if request.method == 'POST':
        id = request.form['firstname']
        mdp = request.form['password']
        print('ID: ', id, ', MDP: ', mdp)

        # on cherche user selon id et mdp
        # d'abord les eleves
        user = None
        for el in Eleve.query.all():
            if el.idEl == int(id):
                if el.mdpEl == mdp:
                    user = "E",el.nomEl, el.prenomEl, el.idEl
                    break
        # puis les moniteurs
        for mo in Moniteur.query.all():
            if mo.idMo == int(id):
                if mo.mdpMo == mdp:
                    user = "M",mo.nomMo, mo.prenomMo, mo.idMo
                    break
        print("user", user)
        if user == None:
            flash("La connexion ne peut aboutir", "error")
            return render_template("home.html")
        else:
            flash("connexion autorisé", 'success')

            if user[0] == 'E':
                return redirect(url_for('eleve', id=user[3]))
            elif user[0] == 'M':
                return redirect(url_for('moniteur', id=user[3]))
    else:
        return render_template("home.html")

@app.route("/", methods=("get", "post"))
def home():
    return render_template("home.html")

# Pages d'accueils selon connexion

@app.route ("/eleve/<int:id>", methods=("get", "post"))
def eleve (id):
    req = el_voir_cours(id)
    return render_template('eleve.html', id=id, req=req, el=get_eleve(id))

@app.route ("/moniteur/<int:id>", methods=("get", "post"))
def moniteur (id):
    req = mo_voir_cours(id)
    return render_template('moniteur.html', id=id, req=req, mo=get_moniteur(id), eleves=liste_el_par_cours())


# Les Routes des elèves

@app.route ("/eleve/<int:id>/reserver-cours", methods=("get", "post"))
def el_reserver_cours(id):
    req = el_voir_diff_cours(id)
    return render_template("el-reserver-cours.html", id=id, req=req, el=get_eleve(id))

@app.route ("/eleve/<int:id>/reserver-cours/date", methods=("get", "post"))
def recherche_cours_date(id):
    """Voir les cours selon unee date précise"""
    if request.method == 'POST':
        rech = request.form['recherche']
        if rech == "":
            req =0
        else:
            req = el_voir_diff_cours_rech(id, rech)
        if req == []:
            req = 0
        return render_template("el-reserver-cours.html", id=id, req=req, el=get_eleve(id))


# CHOISIR COURS

@app.route ("/eleve/<int:idEl>/payer-cours/<int:idC>", methods=("get", "post"))
def el_choisir_cours (idEl, idC):
    poney = get_poneys_tri_nom()
    return render_template("el-payer-cours.html",idEl=idEl, el=get_eleve(idEl), cours=get_info_cours(idC), idC=idC, poney=poney)

@app.route ("/eleve/<int:id>/reserver-cours/<int:idC>", methods=("get", "post"))
def el_payer_reserver_cours (id, idC):
    if request.method == 'POST':
        idPo = int(request.form['poney'])
        el_reserver_un_cours(id, idC, idPo)
        return redirect(url_for('el_reserver_cours', id=id))


# PAYER

@app.route ("/pouet/<id>", methods=("get", "post"))
def el_payer_cotisation(id):
    # return render_template("payer.html", id=id, idC=5)
    el_payer_coti(id)
    return redirect(url_for('eleve', id=id))

@app.route ("/pouetpouet/<id>/<idC>", methods=("get", "post"))
def el_payer_cours(id,idC):
    # return render_template("payer.html", id=id, idC=idC)
    el_payer_c(id, idC)
    return redirect(url_for('eleve', id=id))


# Les routes des pages d'informations

@app.route ("/voir-poneys/<int:id>", methods=("get", "post"))
def voir_poneys(id):
    return render_template("voir-poneys.html", id=id, poneys=get_poneys(),rr=get_robe_et_race())

@app.route ("/voir-eleves/<int:id>", methods=("get", "post"))
def voir_eleves(id):
    return render_template("voir-eleves.html", id=id, eleves=get_eleves())

@app.route ("/voir-cours/<int:id>", methods=("get", "post"))
def voir_cours(id):
    return render_template("voir-cours.html", id=id, req=get_info_all_cours(), eleves=liste_el_par_cours(), poneys=liste_poneys_par_cours(), moniteurs=get_moniteurs_tri_nom(), el=get_eleves_tri_nom(), po=get_poneys_tri_nom())

@app.route ("/voir-moniteurs/<int:id>", methods=("get", "post"))
def voir_moniteurs(id):
    return render_template("voir-moniteurs.html", id=id, moniteurs=get_moniteurs())


# Les routes de Création de Données

@app.route ("/admin/<int:id>/create_poney/", methods=("get", "post"))
def create_poney(id):
    if request.method == 'POST':
        nom = request.form['nom']
        poids = request.form['poids']
        age = request.form['age']
        robe = request.form['robe']
        race = request.form['race']
        etatSante = request.form['etatSante']

        po_create_poney(nom, poids, age, robe, race, etatSante)
        return redirect(url_for('voir_poneys', id=id))

@app.route ("/admin/<int:id>/create_eleve", methods=("get", "post"))
def create_eleve(id):
    if request.method == 'POST':
        nom = request.form['nom']
        prenom = request.form['prenom']
        poids = request.form['poids']
        age = request.form['age']
        niveau = request.form['niveau']
        cotisation = request.form['cotisation']

        el_create_eleve(nom, prenom, poids, age, niveau, cotisation)
        return redirect(url_for('voir_eleves', id=id))

@app.route ("/admin/<int:id>/create_cours", methods=("get", "post"))
def create_cours(id):
    if request.method == 'POST':
        moniteur = request.form['moniteur']
        date = request.form['date']
        duree = request.form['duree']
        activite = request.form['activite']
        eleve = request.form['eleve']
        poney = request.form['poney']
        prix = request.form['prix']

        co_create_cours(moniteur, date, duree, activite, eleve, poney, prix)
        return redirect(url_for('voir_cours', id=id))

@app.route ("/admin/<int:id>/create_moniteur", methods=("get", "post"))
def create_moniteur(id):
    if request.method == 'POST':
        nom = request.form['nom']
        prenom = request.form['prenom']
        poids = request.form['poids']
        age = request.form['age']
        niveau = request.form['niveau']
        salaire = request.form['salaire']

        mo_create_moniteur(nom, prenom, poids, age, niveau, salaire)
        return redirect(url_for('voir_moniteurs', id=id))