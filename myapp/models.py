from .app import db
from sqlalchemy.sql.expression import func

# Partie Tables de la BD

class Eleve( db.Model):

    __tablename__ = 'ELEVE'

    idEl = db.Column(db.Integer, primary_key=True)
    mdpEl = db.Column(db.String(30))
    nomEl = db.Column(db.String(30))
    prenomEl = db.Column(db.String(30))
    poidsEl = db.Column(db.Float)
    ageEl = db.Column(db.Integer)
    niveauEl = db.Column(db.Integer)
    cotisationAnn = db.Column(db.Integer)
    # le lien aux reservations
    res_eleve = db.relationship("Reservation", back_populates="eleve")


    # def __repr__(self) -> str:
    #     if self.cotisationAnn == 0:
    #         cot = True
    #     else:
    #         cot = False
    #     return "< Eleve (%i) %n %p, age: %a poids: %p niveau: %n cotisation annuelle: %c>" % (
    #         self.idEl,self.nomEln, self.prenomEl, self.ageEl, self.poidsEl, self.niveauEl, cot)

class Moniteur(db.Model):

    __tablename__ = 'MONITEUR'

    idMo = db.Column(db.Integer, primary_key=True)
    mdpMo = db.Column(db.String(30))
    nomMo = db.Column(db.String(30))
    prenomMo = db.Column(db.String(30))
    poidsMo = db.Column(db.Float)
    ageMo = db.Column(db.Integer)
    niveauMo = db.Column(db.Integer)
    salaire = db.Column(db.Float)
    # def __repr__(self) -> str:
    #     return "< Moniteur (%i) %n %p, age: %a poids: %p niveau: %n salaire: %s >" % (
    #         self.idMo,self.nomMo, self.prenomMo, self.ageMo, self.poidsMo, self.niveauMo, self.salaire)

class Cours(db.Model):

    __tablename__ = 'COURS'

    idC = db.Column(db.Integer, primary_key=True)
    idMo = db.Column(db.Integer, db.ForeignKey("MONITEUR.idMo"))
    date = db.Column(db.DateTime(50))
    duree = db.Column(db.Integer)
    activite = db.Column(db.String(20))
    nbPers = db.Column(db.Integer)
    prixCours = db.Column(db.Float)
    # le moniteur du cours et le lien aux reservations
    moniteur = db.relationship('Moniteur', backref=db.backref('cours', lazy="dynamic"))
    res_cours = db.relationship("Reservation", back_populates="cours")

    # def __repr__(self) -> str:
    #     return "< Cours (%i)(moniteur: %m) %d duree: %d activite: %a nbPers: %n prix: %p >" % (
    #         self.idC,self.idMo, self.date, self.duree, self.activite, self.nbPers, self.prixCours)

class Poney(db.Model):

    __tablename__ = 'PONEY'

    idPo = db.Column(db.Integer, primary_key=True)
    nomPo = db.Column(db.String(30))
    poidsAccepte = db.Column(db.String(30))
    agePo = db.Column(db.Integer)
    robePo = db.Column(db.String(20))
    racePo = db.Column(db.String(20))
    etatSante = db.Column(db.String(20))
    tpsAvantRepos = db.Column(db.Integer)
    # le lien aux reservations
    res_poney = db.relationship("Reservation", back_populates="poney")

    # def __repr__(self) -> str:
    #     return "< Poney (%i) %n, poids max accepté: %p age: %a  robe: %r race: %r etat de santé: %e tps avant repos :%t >" % (
    #         self.idPo,self.nomPo, self.poidsAccepte, self.agePo, self.robePo, self.racePo, self.etatSante, self.tpsAvantRepos)

class Reservation(db.Model):

    __tablename__ = 'RESERVATION'

    idR = db.Column(db.Integer, primary_key=True)
    idEl = db.Column(db.Integer, db.ForeignKey("ELEVE.idEl"))
    idPo = db.Column(db.Integer, db.ForeignKey("PONEY.idPo"))
    idC = db.Column(db.Integer, db.ForeignKey("COURS.idC"))
    estPaye = db.Column(db.Integer)
    # l'eleve, le poney et le cours lié à la reservation
    eleve = db.relationship("Eleve", back_populates="res_eleve")
    poney = db.relationship("Poney", back_populates="res_poney")
    cours = db.relationship("Cours", back_populates="res_cours")
    
    def __init__(self,idR, idEl, idPo, idC, estPaye) -> None:
        self.idR = idR
        self.idEl = idEl
        self.idPo = idPo
        self.idC = idC
        self.estPaye = estPaye
        
    # def __repr__(self) -> str:
    #     if self.estPaye == 0:
    #         paye = True
    #     else:
    #         paye = False
    #     return "< Reservation (%i) (eleve: %e) (poney: %p) (cours: %c) est payé: %p >" %(
    #         self.idR, self.idEl, self.idPo, self.idC, paye)



# Parties Fonctions

# ------------------------------------------------------------------------------------------------

#   ______ _                     
#  |  ____| |                    
#  | |__  | | _____   _____  ___ 
#  |  __| | |/ _ \ \ / / _ \/ __|
#  | |____| |  __/\ V /  __/\__ \
#  |______|_|\___| \_/ \___||___/
    
# getteurs
def get_eleve(id):
    return Eleve.query.filter(Eleve.idEl == id).first()

def get_eleves():
    return Eleve.query.order_by(Eleve.idEl).all()

def get_eleves_tri_nom():
    return Eleve.query.order_by(Eleve.nomEl).all()

def get_id_max_eleve():
    # scalar pour passer du type querry à un type traitable ( ici int )
    return db.session.query(func.max(Eleve.idEl)).scalar()


#payer
def el_payer_coti(id):
    el = Eleve.query.filter(Eleve.idEl==id).first()
    el.cotisationAnn = 0
    db.session.commit()

def el_payer_c(idEl, idC):
    res = Reservation.query.filter(Reservation.idEl==idEl, Reservation.idC==idC).first()
    res.estPaye = 0
    db.session.commit()


# Création d'un nouvel eleve
def el_create_eleve(nom, prenom, poids, age, niveau, cotisation):
    if cotisation == "non":
        cotisation = 1
    else:
        cotisation = 0
    eleve = Eleve(idEl=get_id_max_eleve()+1, mdpEl="mdpe", nomEl=nom, prenomEl=prenom, poidsEl=poids, ageEl=age, niveauEl=niveau, cotisationAnn=cotisation)
    db.session.add(eleve)
    db.session.commit()

# Réserver un cours
def el_reserver_un_cours(idEl, idC, idPo):
    """Permet à l'Eleve "idEl" de reserver le Cours "idC" """
    reservation = Reservation(get_id_max_reserv()+1, idEl=idEl, idPo=idPo, idC=idC, estPaye=1  )
    c = Cours.query.filter(Cours.idC == idC).one()
    c.nbPers += 1
    db.session.add(reservation)
    db.session.commit()

# ------------------------------------------------------------------------------------------------

#    _____                     
#   / ____|                    
#  | |     ___  _   _ _ __ ___ 
#  | |    / _ \| | | | '__/ __|
#  | |___| (_) | |_| | |  \__ \
#   \_____\___/ \__,_|_|  |___/                       

#getteurs
def get_cours(id):
    return Cours.query.filter(Cours.idC == id).first()

def get_cours():
    return Cours.query.all()

def get_id_max_cours():
    return db.session.query(func.max(Cours.idC)).scalar()

def get_info_cours(idC):
    """retourne toutes les infos d'un Cours "idC" """
    return db.session.query(Cours, Reservation, Moniteur). \
        select_from(Cours).join(Reservation).join(Moniteur).filter(Cours.idC == idC).all()

def get_info_all_cours():
    """retourne toutes les infos de tous les cours"""
    return db.session.query(Cours, Reservation, Moniteur). \
        select_from(Cours).join(Reservation).join(Moniteur).order_by(Cours.idC).all()


#cours pour un eleve
def el_voir_cours(id):
    """retourne toutes les infos des cours d'un eleve"""
    return db.session.query(Eleve, Reservation, Cours, Poney, Moniteur). \
        select_from(Eleve).join(Reservation).join(Cours).join(Poney).join(Moniteur).filter(Eleve.idEl == id).all()

def el_voir_diff_cours(id):
    """retourne toutes les infos de tout les cours auxquel ne participe pas un eleve"""
    return db.session.query(Eleve, Reservation, Cours, Poney, Moniteur). \
        select_from(Eleve).join(Reservation).join(Cours).join(Poney).join(Moniteur).filter(Eleve.idEl != id).order_by(Cours.date).all()

def el_voir_diff_cours_rech(id, rech):
    """même chose que el_voir_diff_cours mais pour une date précise"""
    return db.session.query(Eleve, Reservation, Cours, Poney, Moniteur). \
        select_from(Eleve).join(Reservation).join(Cours).join(Poney).join(Moniteur).filter(Eleve.idEl != id).filter(Cours.date==rech).all()

#cours pour un moniteur
def mo_voir_cours(id):
    """retourne toutes les infos des cours d'un moniteurs"""
    return db.session.query(Moniteur, Cours, Reservation, Poney, Eleve). \
        select_from(Moniteur).join(Cours).join(Reservation).join(Poney).join(Eleve).filter(Moniteur.idMo== id).all()


# Création d'un nouveau cours
def co_create_cours(moniteur, date, duree, activite, eleve, poney, prix):
    nbPers = 0
    if eleve != 0:
        nbPers = 1
    cours = Cours(idC=get_id_max_cours()+1, idMo=moniteur, date=date, duree=duree, activite=activite, nbPers=nbPers, prixCours=prix)
    db.session.add(cours)
    if nbPers == 1 and poney != 0:
        el_reserver_un_cours(eleve, get_id_max_cours(), poney)
    else:
        el_reserver_un_cours(None, get_id_max_cours(), None)
    # le commit se trouve directement dans el_reserver_un_cours

# ------------------------------------------------------------------------------------------------

#   _____                           
#  |  __ \                          
#  | |__) |__  _ __   ___ _   _ ___ 
#  |  ___/ _ \| '_ \ / _ \ | | / __|
#  | |  | (_) | | | |  __/ |_| \__ \
#  |_|   \___/|_| |_|\___|\__, |___/
#                          __/ |    
#                         |___/     

#getteurs
def get_poneys():
    return Poney.query.order_by(Poney.idPo).all()

def get_poneys_tri_nom():
    return Poney.query.order_by(Poney.nomPo).all()

def get_id_max_poney():
    return db.session.query(func.max(Poney.idPo)).scalar()

# Création d'un nouveau poney
def po_create_poney(nom, poids, age, robe, race, etatSante):
    poney = Poney(idPo=get_id_max_poney()+1, nomPo=nom, poidsAccepte=poids, agePo=age, robePo=robe, racePo=race, etatSante=etatSante, tpsAvantRepos=0)
    db.session.add(poney)
    db.session.commit()

# ------------------------------------------------------------------------------------------------

#   __  __             _ _                      
#  |  \/  |           (_) |                     
#  | \  / | ___  _ __  _| |_ ___ _   _ _ __ ___ 
#  | |\/| |/ _ \| '_ \| | __/ _ \ | | | '__/ __|
#  | |  | | (_) | | | | | ||  __/ |_| | |  \__ \
#  |_|  |_|\___/|_| |_|_|\__\___|\__,_|_|  |___/
                                              
#getteurs
def get_moniteur(id):
    return Moniteur.query.filter(Moniteur.idMo==id).first()

def get_moniteurs():
    return Moniteur.query.order_by(Moniteur.idMo).all()

def get_moniteurs_tri_nom():
    return Moniteur.query.order_by(Moniteur.nomMo).all()

def get_id_max_moniteur():
    return db.session.query(func.max(Moniteur.idMo)).scalar()


# Création d'un nouveau moniteur
def mo_create_moniteur(nom, prenom, poids, age, niveau, salaire):
    moniteur = Moniteur(idMo=get_id_max_moniteur()+1, mdpMo="mdpm",nomMo=nom, prenomMo=prenom, poidsMo=poids, ageMo=age, niveauMo=niveau, salaire=salaire)
    db.session.add(moniteur)
    db.session.commit()

# ------------------------------------------------------------------------------------------------

#   _____                                _   _                 
#  |  __ \                              | | (_)                
#  | |__) |___  ___  ___ _ ____   ____ _| |_ _  ___  _ __  ___ 
#  |  _  // _ \/ __|/ _ \ '__\ \ / / _` | __| |/ _ \| '_ \/ __|
#  | | \ \  __/\__ \  __/ |   \ V / (_| | |_| | (_) | | | \__ \
#  |_|  \_\___||___/\___|_|    \_/ \__,_|\__|_|\___/|_| |_|___/                                                         

# getteur
def get_id_max_reserv():
    return db.session.query(func.max(Reservation.idR)).scalar()


# ------------------------------------------------------------------------------------------------

# Fonctions de listes des tables de la BD
def liste_el_par_cours():
    """retourne la liste de tous les éleves pour chaque cours
       indice 0 => cours idc=1"""
    res = list()
    for c in Cours.query.all():
        el = db.session.query(Eleve, Reservation). \
            select_from(Eleve).join(Reservation).filter(Reservation.idC == c.idC).all()
        res.append(el)
    return res

def liste_poneys_par_cours():
    """retourne la liste de tous les poneys pour chaque cours
        indice 0 => cours idc=1"""
    res = list()
    for c in Cours.query.all():
        poney = db.session.query(Poney, Reservation). \
            select_from(Poney).join(Reservation).filter(Reservation.idC == c.idC).all()
        res.append(poney)
    return res

def get_robe_et_race():
    """retourne une liste avec toutes les robes et races des poneys"""
    return db.session.query(Poney.robePo).select_from(Poney).group_by(Poney.robePo).all(), db.session.query(Poney.racePo).select_from(Poney).group_by(Poney.racePo).all()
