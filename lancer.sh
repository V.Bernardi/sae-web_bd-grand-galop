virtualenv -p python venv
source venv/bin/activate
pip install -r requirements.txt
gnome-terminal --title=Flask --tab -- flask run; sleep 5; firefox http://127.0.0.1:5000/
