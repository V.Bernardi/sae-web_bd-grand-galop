create table ELEVE(
	idEl int,
	mdpEl varchar(30),
	nomEl varchar(30),
	prenomEl varchar(30),
	poidsEl decimal,
	ageEl int,
	niveauEl int,
	cotisationAnn int check (cotisationAnn between 0 and 1),
	PRIMARY KEY (idEl)
);

create table MONITEUR (
	idMo int,
	mdpMo varchar(30),
	nomMo varchar(30),
	prenomMo varchar(30),
	poidsMo decimal,
	ageMo int,
	niveauMo int,
	salaire decimal,
	PRIMARY KEY (idMo) 
);

create table COURS(
	idC int,
	idMo int,
	date date,
	duree int,
	activite varchar(20),
	nbPers int,
	prixCours decimal,
	PRIMARY KEY (idC),
	FOREIGN KEY (idMo) REFERENCES MONITEUR (idMo)
);	

create table PONEY(
	idPo int,
	nomPo varchar(30),
	poidsAccepte int,
	agePo int,
	robePo varchar(20),
	racePo varchar(20),
	etatSante varchar(20),
	tpsAvantRepos int,
	PRIMARY KEY (idPo)
);

create table RESERVATION(
	idR int,
	idEl int,
	idPo int,
	idC int,
	estPaye int check (estPaye between 0 and 1),
	PRIMARY KEY(idR),
	FOREIGN KEY (idEl) REFERENCES ELEVE (idEl),
	FOREIGN KEY (idPo) REFERENCES PONEY (idPo),
	FOREIGN KEY (idC) REFERENCES COURS (idC)
);


-- A Parir d'ici : les TRIGGER

-- pour chaque nouvelle reservation on fait nbPers +1 en verifiant que < 10
-- les poneys peuvent porter jusqu'a à un poids max
-- Les poneys doivent se reposer 1 heure toutes les 2 h de cours
-- delimiter |
-- create or replace trigger nbPersMaxCours before insert on RESERVATION foreachrow
-- 	begin
-- 	declare nbPers;
-- 	declare idCours;
-- 	declare poidsEleve;
-- 	declare poidsPoney;
-- 	declare mesvarchar(100);
-- 	select idC into idCours from RESERVATION natural join COURS where idR = new.idR;
-- 	select nbPers into nbPers from COURS where idC = idCours;
-- 	if nbPers+1 >  10 then
-- 		set mes=concat('Rerservation impossible, le cours : ',idCours,' est au complet');
-- 		signal SQLSTATE '45000' set MESSAGETEXT=mes;
-- 	end if;

-- 	select poidsEl into poidsEleve from RESERVATION natural join ELEVE where idR = new.idR;
-- 	select poidsAccepte into poidsPoney from RESERVATION natural join PONEY where idR = new.idR;
-- 	if poidsEleve > poidsPoney then
-- 		set mes=concat('Rerservation impossible, le poney ne peut pas supporter l eleve');
-- 		signal SQLSTATE '45000' set MESSAGETEXT=mes;
-- 	end if; 
-- end |
-- delimiter;

-- -- même chose pour l'update
-- create or replace trigger nbPersMaxCours before update on COURS foreachrow
-- 	begin
-- 	declare nbPers;
-- 	declare mesvarchar(100);
-- 	select nbPers into nbPers from COURS where idC = new.idC;
-- 	if nbPers+1 >  10 then
-- 		set mes=concat('Il y a déjà assez de personne dans le cours: ',idCours);
-- 		signal SQLSTATE '45000' set MESSAGETEXT=mes;
-- 	end if;
-- end |


-- -- la duree ne doit pas être > 2
-- delimiter |
-- create or replace trigger nbPersMaxCours before insert on COURS foreachrow
-- 	begin
-- 	declare duree;
-- 	declare mesvarchar(100);
-- 	select duree into duree from COURS where idC = new.idC;
-- 	if duree >  2 then
-- 		set mes=concat('Erreur ! La duree ne doit pas être > 2: ');
-- 		signal SQLSTATE '45000' set MESSAGETEXT=mes;
-- 	end if;
-- end |
-- delimiter;
-- -- même chose pour l'update
-- delimiter |
-- create or replace trigger nbPersMaxCours before update on COURS foreachrow
-- 	begin
-- 	declare duree;
-- 	declare mesvarchar(100);
-- 	select duree into duree from COURS where idC = new.idC;
-- 	if duree >  2 then
-- 		set mes=concat('Erreur ! La duree ne doit pas être > 2: ');
-- 		signal SQLSTATE '45000' set MESSAGETEXT=mes;
-- 	end if;
-- end |
-- delimiter;