import random

# Fichier pour générer les lignes d'insertions de la base de donnée, automatiquement

def creer_param_eleve(n, id):
    """
    parametre n : nombre de lignes à ajouter
    id : id max des eleves dans la base
    """
    liste_nom = ["Martin", "Bernard", "Thomas", "Petit", "Robert", "Richard", "Durand", "Dubois", "Moreau", "Laurent", "Simon", "Michel", "Lefebvre", "Leroy", "Roux", "David", "Bertrand", "Morel", "Fournier", "Girard", "Bonnet", "Dupont", "Lambert", "Fontaine", "Rousseau", "Vincent", "Muller", "Lefevre", "Faure", "Andre", "Mercier", "Blanc", "Guerin", "Boyer", "Garnier", "Chevalier", "Francois", "Legrand", "Gauthier", "Garcia"]
    liste_prenom = ["Léo", "Ibrahim", "Gabriel", "Gaspard", "Raphaël", "Sohan", "Arthur", "Clément", "Louis", "Mathéo", "Adam",	"Simon", "Maël", "Baptiste", "Lucas", "Maxence", "Hugo", "Imran", "Noah", "Kaïs", "Jade", "Clémence", "Louise", "Théa", "Emma","Elena", "Alice", "Alba", "Ambre", "Emy", "Lina", "Clara", "Rose", "Lana", "Chloe", "Aya", "Mia", "Lyna", "Léa", "Yasmine"]

    ligne_res = ""
    for _ in range(n):
        id += 1
        ligne_elv = "    ("+str(id)+", \""+liste_nom[random.randint(0, len(liste_nom)-1)]+"\", \""+liste_prenom[random.randint(0, len(liste_prenom)-1)]+"\", "+str(random.randint(20,100))+"."+str(random.randint(0,99))+", "+str(random.randint(8,70))+", "+str(random.randint(0,10))+", "+str(random.randint(0,1))+"),"
        ligne_res += ligne_elv + "\n"
    print(ligne_res)

def creer_param_moni(n, id):
    """
    parametre n : nombre de lignes a ajouter
    id : id max des moniteurs dans la base
    """
    liste_nom = ["Martin", "Bernard", "Thomas", "Petit", "Robert", "Richard", "Durand", "Dubois", "Moreau", "Laurent", "Simon", "Michel", "Lefebvre", "Leroy", "Roux", "David", "Bertrand", "Morel", "Fournier", "Girard", "Bonnet", "Dupont", "Lambert", "Fontaine", "Rousseau", "Vincent", "Muller", "Lefevre", "Faure", "Andre", "Mercier", "Blanc", "Guerin", "Boyer", "Garnier", "Chevalier", "Francois", "Legrand", "Gauthier", "Garcia"]
    liste_prenom = ["Léo", "Ibrahim", "Gabriel", "Gaspard", "Raphaël", "Sohan", "Arthur", "Clément", "Louis", "Mathéo", "Adam",	"Simon", "Maël", "Baptiste", "Lucas", "Maxence", "Hugo", "Imran", "Noah", "Kaïs", "Jade", "Clémence", "Louise", "Théa", "Emma","Elena", "Alice", "Alba", "Ambre", "Emy", "Lina", "Clara", "Rose", "Lana", "Chloe", "Aya", "Mia", "Lyna", "Léa", "Yasmine"]
    
    ligne_res = ""
    for _ in range(n):
        id += 1       
        ligne_moni = "    ("+str(id)+", \""+liste_nom[random.randint(0, len(liste_nom)-1)]+"\", \""+liste_prenom[random.randint(0, len(liste_prenom)-1)]+"\", "+str(random.randint(45,100))+"."+str(random.randint(0,90))+", "+str(random.randint(16,70))+", "+str(random.randint(3,10))+", "+str(random.randint(1000,2000))+"),"
        ligne_res += ligne_moni + "\n"

    print(ligne_res)
    
def creer_param_poney(n, id):
    """
    parametre n : nombre de lignes à ajouter
    id : id max des poneys dans la base
    """
    liste_nom = ["Aldino", "Amington", "Atsoung", "Babytrote", "Biluochon", "Birdy", "Chifoumi", "Chipote", "Citrouille", "Dardar", "Dardji", "Dodelina", "Doucine", "Falbala", "Flipette", "Gibouille", "Goolging", "Jazz", "Karabistouille", "Kulbute", "Lapsang", "Laptop", "Loutz", "Madness", "Marsupilami", "Mélusine", "Minitrote", "Mistik", "Moskito", "Natchaï", "Paddington", "Padling", "Patinette", "Pistouille", "Radistuk", "Rok’n’up", "Saddling", "Sadeltop", "Sirkiti", "Souchong"] 
    liste_robe = ["alezan brûlé", "café au lait", "bai", "isabelle", "souris", "blanc", "gris", "crème", "chocolat" ]
    liste_race = ["Jiangchang", "Pequeno", "Poney Bosniaque", "Hackney", "Poney Boer", "Haflinger", "Anadolu", "Cob irlandais", "Fjord"]
    liste_sante = ["Excellente santé", "Bonne santé", "Santé moyenne", "Santé dégradée", "Santé critique"]

    ligne_res = ""
    j = len(liste_nom)
    for _ in range(n):
        id += 1
        j -= 1
        ligne = "    ("+str(id)+", \""+liste_nom.pop(random.randint(0, j))+"\", "+str(random.randint(30,100))+", "+str(random.randint(5,26))+", \""+  liste_robe[random.randint(0, len(liste_robe)-1)] + "\", \"" + liste_race[random.randint(0, len(liste_race)-1)] + "\", \"" + liste_sante[random.randint(0, len(liste_sante)-1)] + "\", " +str(random.randint(0,1))+"),"
        ligne_res += ligne + "\n"


    print(ligne_res)

def creer_param_cours(n, id, idMo, idMoMax):
    """
    parametre n : nombre de lignes à ajouter
    idR : id max du cours
    idMo, idMoMax : le min et max d'id du moniteur
    """
    liste_activite = ["saut d'obstacle", "slalom", "ski en longueur", "initiation", "course", "dressage"]
    
    ligne_res = ""
    for _ in range(n):
        id += 1
        ligne = "    ("+str(id)+", "+str(random.randint(idMo,idMoMax))+", STR_TO_DATE(\"" +str(random.randint(1,30))+"-"+str(random.randint(1,12))+"-"+str(random.randint(2023,2025))+", "+str(random.randint(8,18))+"-"+str(random.randint(0,60))+"\", \"%d-%m-%Y, %H-%i\"), "+str(random.randint(1,5))+", \""+liste_activite[random.randint(0, len(liste_activite)-1)]+"\", 0, "+str(random.randint(10,200))+"),"
        ligne_res += ligne + "\n"

    print(ligne_res)

def creer_param_reserv(n, idR, idEl, idPo, idC):
    """
    parametre n : nombre de lignes à ajouter
    idR : id max de la reservation
    idEl, idPo, idC : id max des Eleve, Poney, et Cours dans la base
    """

    ligne_res = ""
    for _ in range(n):
        idR, idC = idR+1, idC+1
        ligne = "    ("+str(idR)+", " +str(random.randint(1,idEl))+", "+str(random.randint(1,idPo))+", "+ str(idC)+", "+str(random.randint(0,1))+"),"
        ligne_res += ligne + "\n"

    print(ligne_res)
    
print("\n", "insert into ELEVE values")
creer_param_eleve(100, 0)
print("\n", "insert into MONITEUR values")
creer_param_moni(100, 1000)
print("\n", "insert into PONEY values")
creer_param_poney(35, 0)
print("\n", "insert into COURS values")
creer_param_cours(150, 0, 1001, 1101)
print("\n", "insert into RESERVATION values")
creer_param_reserv(150, 0, 101, 36, 0)
