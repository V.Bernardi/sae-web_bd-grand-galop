# SAE Web_BD Grand Galop

# Equipe du projet :
Corentin FOUQUE et Vincent BERNARDI

## Lien pour le diagramme de la BD

https://app.diagrams.net/?src=about#G1jrBqzlZLnlqelLAOSQmj7ln7NhY-D0QN


**Partie web**
  =
## Fonctionnalités implémentées
### Pour un Visiteur
- Voir les Poneys
- Voir un apperçu du club ( lien externe )
- Voir les nouvelles du club ( lien externe )
- Voir Comment s'occuper s'un poney ( lien externe )
--------------
### Pour les Eleves
 
_Se connecter_

id : 1 , mdp : mdpe

id : 2 , mdp : mdpe

id : 3 , mdp : mdpe

- **Même fonctionnalités que les Visiteurs**
- Voir ses cours
- Payer ses cours et sa cotisation annuelle (de manière fictive)
- Voir les cours
  - Trier les cours par date
  - Réserver un cours
  - Choisir un poney pour un cours

--------------


### Pour les Moniteurs

_Se connecter_

id : 1001 , mdp : mdpm

id : 1003 , mdp : mdpm

id : 1004 , mdp : mdpm
- **Même fonctionnalités que les Visiteurs**
- Voir ses cours
- Voir les Eleves
- Voir les Cours
- Voir les Moniteurs
--------------

### Pour l'Admin

_Se connecter_

id : 1002 , mdp : mdpm
- **Même fonctionnalités que les Visiteurs et moniteurs**
- Ajouter un Cours
- Ajouter un Eleve
- Ajouter un Poney
- Ajouter un Moniteur
--------------
--------------
## Lancement de l'appli web
**Utilisation automatique** : Dans le terminal : Allez dans le dossier racine "sae-web_bd-grand-galop" et entrez la commande suivante.

**note** : le processus prend environ 1min30 sur les pcs de l'IUT 

./lancer.sh

**utilisation manuelle** : Dans le terminal : Allez dans le dossier racine "sae-web_bd-grand-galop" et entrez les commandes suivante.

virtualenv -p python venv

source venv/bin/activate

pip install -r requirements.txt

flask run
