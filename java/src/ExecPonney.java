// Avant de lancer ce fichier, veuillez remplir la ligne 22
// Pour compiler puis executer ce fichier allez dans un terminal et rentrez les commandes suivantes:
// javac *.java         puis        java ExecPonney
// Suivez alors les Instructions pour utiliser l'application !
// Voici respectivement des identifiants d'Eleve et de Moniteur : 1     1001

import java.util.*;
import java.io.*;


public class ExecPonney {
    public static void main(String[] args) {
        Console cnsl1 = System.console();

        if (cnsl1 == null) {
            System.out.println(
                    "No console available");
            return;
        }

        // Rentrez les infos de votre BD et votre environnement 
        ConnexionMySQL co = new ConnexionMySQL("nomServeur", "nomBase", "nomLogin", "mdp");
        Requetebd req = new Requetebd(co);
                                                                                   
        System.out.println("Bienvenue sur l'appli grand galop !");
        System.out.println("Vous Pourrez quitter nimporte quelle étape en appuyant sur q");
        System.out.println("||| ---------------------------------------- |||\n");                                                                           
        System.out.println("Veuillez tout d'abord vous connecter");

        // Connexion de l'utilisateur à la BD
        String idU; Integer idUser =0;
        String role = "0";
        while (true) {
            idU = cnsl1.readLine("Entrez votre identifiant : ");
            if (idU.charAt(0) =='q'){
                break;
            }
            try {
                idUser = Integer.parseInt(idU);
                role = req.getRole(idUser);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Votre identifiant ne fonctionne pas, veuillez réessayé");
            }
        }

        Eleve eleve = new Eleve(0, "paul", "andre", 0, 0, 0, 0);
        Moniteur moni = new Moniteur(0, "andre", "paul", 0, 0, 0, 0);
        try {
            if (role == "Eleve"){
                eleve = req.getEleve(idUser);
            }else{
                moni = req.getMoniteur(idUser);
            }
        } catch (Exception e) {
            System.out.println("Erreur dans l'attribution de role");
        }
        

        while (true){
            // On créer l'object console
            Console cnsl = System.console();

            if (cnsl == null) {
                System.out.println(
                        "No console available");
                return;
            }


            System.out.println("\nChoisissez une option :  --------------------------");
            if (role == "Eleve"){
                System.out.println("1/  Voir mes Cours");
                System.out.println("2/  Reserver un Cours");
                System.out.println("3/  Payer un Cours");
                
                String ligne = "4/ Payer ma cotisation annuelle";
                if (eleve.getCotisationAnn() == true){
                    ligne += "déjà fait";
                }else {
                    ligne += "à faire"; 
                }
                System.out.println(ligne);
                System.out.println("q/ Quitter");
                
                String input="",input2;
                Integer inp=0;
                boolean continu = true;
                
                while (continu){
                    input = cnsl.readLine("\nEntrer votre choix: ");
                    if (Arrays.asList("1","2","3","4","5").contains(input)){
                        continu = false;
                        inp = Integer.parseInt(input);
                    }else if (Arrays.asList("q", "p").contains(input)){
                        continu = false;
                    }else{
                        System.out.println("Veuillez rentrez un nombre entre 1 et 5 (compris)");
                    }
                }
                
                if (inp == 1){ //Voir mes Cours
                    System.out.println("   ----   Voir mes Cours   ----   ");
                    //on affiche les cours
                    List<Cours> lescours = new ArrayList<Cours>();
                    try {
                        lescours = req.affCoursEleve(eleve, false);
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des cours");
                    }
                    for (Cours cours : lescours){
                        System.out.println(cours);
                    }
                    
                }else if (inp ==2){ //Reserver un Cours

                    System.out.println("   ----   Reserver un Cours   ----   ");
                    //afficher les cours ( à partir du jour actuel )
                    List<Cours> lescours = new ArrayList<Cours>();
                    try {
                        lescours = req.affCoursEleve(eleve, false);
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des cours");
                    }
                    for (Cours cours : lescours){
                        System.out.println(cours);
                    }
                    String lesPoney = ""; int i=0;
                    try {
                        for (Poney po : req.affPoneys()){
                            lesPoney += po.getNomPo() + " " + po.getIdPo() + " ";
                            if (i%3==0){ lesPoney += "\n"; i++;}
                        }
                        System.out.println(lesPoney);
                    } catch (Exception e) {
                        System.out.println("Erreur dans l'affichage des poneys");
                    }

                    while (true){
                        input2 = cnsl.readLine("\nChoisissez un ID de Cours et celui d'un poney que vous voulez reserver : ");
                        String[] liste = null; liste = input2.split(" ");
                        Integer idCo =0, idPo=0;
                        if (input2.charAt(0) == 'q'){
                            break;
                        }else if (liste.length != 0){
                            try {
                                idCo = Integer.parseInt(liste[0]);
                                idPo = Integer.parseInt(liste[1]);
                            }catch (Exception e) {
                                
                            }
                        }else if (req.getListeIdCours(lescours).contains(Integer.parseInt(input2))){ 
                            try {
                                eleve.reserverCours(idCo, idPo, co);
                                break;
                            } catch (Exception e) {
                                System.out.println("Erreur dans la reservation d'un cours");
                            }
                            
                        }else{
                            System.out.println("Rentrez un ID d'un cours dans la liste");
                        }
                    }

                }else if (inp == 3){ // Payer un Cours
                    System.out.println("   ----   Payer un Cours   ----   ");
                    //afficher les cours de l'eleve non payés( à partir du jour actuel )
                    
                    List<Cours> lescours = new ArrayList<Cours>();
                    try {
                        lescours = req.affCoursEleve(eleve, true);
                        for (Cours cours : lescours){
                            System.out.println(cours);
                        }
                    } catch (Exception e) {
                        System.out.println("Erreur dans l'affichage des cours non payés de l'eleve");
                    }
                    
                    while (true){
                        input2 = cnsl.readLine("\nChoisissez un ID de Cours que vous voulez payer : ");
                        if (input2.charAt(0) =='q'){
                            break;
                    
                        }else if (req.getListeIdCours(lescours).contains(Integer.parseInt(input2))){ 
                            try {
                                eleve.payerCours(Integer.parseInt(input2), co);
                                break;
                            } catch (Exception e) {
                                System.out.println("Erreur en essayant de payer un cours");
                            }
                            
                        }else{
                            System.out.println("Rentrez un ID d'un cours dans votre liste");
                        }
                    }
                    // mini interface pour payer (pour rigoler)

                }else if (inp == 4){ // Payer ma cotisation annuelle
                    System.out.println("   ----   Payer ma cotisation annuelle   ----   ");
                    eleve.payerCotisation();

                    // mini interface pour payer (pour rigoler)

                }else if (input.charAt(0) == 'q'){ // Quitter
                    System.out.println("Bonne journée !");
                    break;
                }else if (input.charAt(0) =='p'){ System.out.println("vive les poney !");}
                else{
                    System.out.println("Erreur dans l'input mdr");
                    System.out.println("1".getClass().getSimpleName());
                }
                
                
            }else{
                System.out.println("1/  Voir mes Cours");
                System.out.println("2/  Voir / Ajouter des Poneys");
                System.out.println("3/  Voir / Ajouter des Cours");
                System.out.println("4/  Voir / Ajouter des éleves");
                System.out.println("5/  Voir / Ajouter des moniteurs");
                System.out.println("q/ Quitter");

                String input="", input2;
                Integer inp = 0;
                boolean continu = true;
                while (continu){
                    input = cnsl.readLine("\nEntrer votre choix: ");
                    if (Arrays.asList("1","2","3","4","5").contains(input)){
                        continu = false;
                        inp = Integer.parseInt(input);
                    }else if (input.charAt(0)=='q'){
                        continu = false;
                    }else{
                        System.out.println("Veuillez rentrez un nombre entre 1 et 5 (compris)");
                    }
                }

                if (inp ==1){ // Voir mes Cours
                    System.out.println("   ----   Voir mes Cours   ----   ");
                    //afficher les cours du moniteur( à partir du jour actuel )
                    List<Cours> lescours = new ArrayList<Cours>();
                    try {
                        lescours = req.affCoursMoniteur(moni);
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des cours du moniteurs");
                    }
                    for (Cours cours : lescours){
                        System.out.println(cours);
                    }

                }else if (inp == 2){ // Voir / Ajouter des Poneys
                    System.out.println("   ----   Voir / Ajouter des Poneys   ----   ");
                    //afficher les poneys
                    String lesPoney = ""; int i=0;
                    List<Poney> poneys = new ArrayList<Poney>();
                    try {
                        poneys = req.affPoneys();
                        for (Poney po : poneys){
                            lesPoney += po.getNomPo() + " ";
                            if (i%5==0){ lesPoney += "\n"; }
                        }
                        System.out.println(lesPoney);
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des poneys");
                    }
                    
                   
                    

                    System.out.println("Ajouter un poney : id nom poidsAccepte age robe race etatSante ");
                    System.out.println("q/  Quitter");

                    Integer idPo, poidsAccepte, agePo;
                    String nomPo, robe, race, etatSante;
                    while (true){
                        input2 = cnsl.readLine("\n Entrer les infos de votre choix : ");
                        String[] liste = null; liste = input2.split(" ");
                        if (input2.charAt(0) == 'q'){
                            break;
                        }else if (liste.length != 0){
                            try {
                                idPo = Integer.parseInt(liste[0]);
                                nomPo = liste[1];
                                poidsAccepte = Integer.parseInt(liste[2]);
                                agePo = Integer.parseInt(liste[3]);
                                robe = liste[4];
                                race = liste[5];
                                etatSante = liste[6];
                                //creer un poney avec ces infos (tps = 0)
                                Poney po = new Poney(idPo, nomPo, poidsAccepte, agePo, robe, race, etatSante,0);
                                req.setPoney(po);
                                System.out.println("le poney a été créé");
                                break;
                            } catch (Exception e) {
                                System.out.println("Merci de rentrer les infos séparées d'un espace ");
                            }
                        }else{
                            System.out.println("Le champs est vide"); 
                        }
                    }
                }else if (inp == 3){ // Voir / Ajouter des Cours
                    System.out.println("   ----   Voir / Ajouter des Cours   ----   ");
                    //afficher les cours( à partir du jour actuel )
                    List<Cours> lescours = new ArrayList<Cours>();
                    try {
                        lescours = req.affCoursMoniteur(moni);
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des cours");
                    }
                    for (Cours cours : lescours){
                        System.out.println(cours);
                    }
                    
                    System.out.println("Ajouter un cours : id jour-mois-annee heure-minute duree activite prixCours");
                    System.out.println("q/  Quitter");

                    Integer idCo, duree, prixCours;
                    String date, activite;
                    while (true){
                        input2 = cnsl.readLine("\n Entrer les infos de votre choix : ");
                        String[] liste = null; liste = input2.split(" ");
                        if (input2.charAt(0) == 'q'){
                            break;
                        }else if (liste.length != 0){
                            try {
                                idCo = Integer.parseInt(liste[0]);
                                date = liste[1] + liste[2];
                                duree = Integer.parseInt(liste[3]);
                                activite = liste[4];
                                prixCours = Integer.parseInt(liste[5]);
                                //creer un cours avec ces infos
                                Cours cours = new Cours(idCo, date, "0",duree, activite, 0, prixCours);
                                req.setCours(cours);
                                System.out.println("cours créé");
                                break;
                            } catch (Exception e) {
                                System.out.println("Merci de rentrer les infos séparées d'un espace ");
                            }
                        }else{
                            System.out.println("Le champs est vide");
                        }
                    }
                }else if (inp ==4){ // Voir / Ajouter des éleves
                    System.out.println("   ----   Voir / Ajouter des éleves   ----   ");
                    //afficher les eleves
                    try {
                        for (Eleve res : req.affEleves()){
                            System.out.println(res);
                        }
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des eleves");
                    }
                    
                    System.out.println("Ajouter un eleve : id nom prenom poids age niveau cotisationAnnuelle=>(0 ou 1)");
                    System.out.println("q/  Quitter");

                    Integer idEl, poidsEl, ageEl, niveauEl, cotisationAnn;
                    String nomEl, prenomEl;
                    while (true){
                        input2 = cnsl.readLine("\n Entrer les infos de votre choix : ");
                        String[] liste = null; liste = input2.split(" ");
                        if (input2.charAt(0) == 'q'){
                            break;
                        }else if (liste.length != 0){
                            try {
                                idEl = Integer.parseInt(liste[0]);
                                nomEl = liste[1];
                                prenomEl = liste[2];
                                poidsEl = Integer.parseInt(liste[3]);
                                ageEl = Integer.parseInt(liste[4]);
                                niveauEl = Integer.parseInt(liste[5]);
                                cotisationAnn= Integer.parseInt(liste[6]);
                                //creer un eleve avec ces infos
                                Eleve el = new Eleve(idEl, nomEl, prenomEl, poidsEl, ageEl, niveauEl, cotisationAnn);
                                req.setEleve(el);
                                System.out.println("eleve créé");
                                break;
                            } catch (Exception e) {
                                System.out.println("Merci de rentrer les infos séparées d'un espace ");
                            }
                        }else{
                            System.out.println("Le champs est vide");
                        }
                    }
                }else if (inp ==5){ // Voir / Ajouter des moniteurs
                    System.out.println("   ----   Voir / Ajouter des moniteurs   ----   ");
                    //afficher les moniteurs
                    try {
                        for (Moniteur res : req.affMoniteur()){
                            System.out.println(res);
                        }
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des moniteurs");
                    }
                    
                    System.out.println("Ajouter un moniteur : id nom prenom poids age niveau salaire");
                    System.out.println("q/  Quitter");

                    Integer idMo, poidsMo, ageMo, niveauMo;
                    String nomMo, prenomMo;
                    double salaire;
                    while (true){
                        input2 = cnsl.readLine("\n Entrer votre choix : ");
                        String[] liste = null; liste = input2.split(" ");
                        if (input2.charAt(0) == 'q'){
                            break;
                        }else if (liste.length != 0){
                            try {
                                idMo = Integer.parseInt(liste[0]);
                                nomMo = liste[1];
                                prenomMo = liste[2];
                                poidsMo = Integer.parseInt(liste[3]);
                                ageMo = Integer.parseInt(liste[4]);
                                niveauMo = Integer.parseInt(liste[5]);
                                salaire= Double.parseDouble(liste[6]);
                                //creer un moniteur avec ces infos
                                
                                Moniteur mo = new Moniteur(idMo, nomMo, prenomMo, poidsMo, ageMo, niveauMo, salaire);
                                req.setMoniteur(mo);
                                System.out.println("moniteur créé");
                                break;
                            } catch (Exception e) {
                                System.out.println("Merci de rentrer les infos séparées d'un espace ");
                            }
                        }else{
                            System.out.println("Le champs est vide");
                        }
                    }
                }else if (inp ==6){ // Voir / Ajouter des reservations
                    System.out.println("   ----   Voir / Ajouter des reservations   ----   ");
                    //afficher les reservations
                    try {
                        for (Reservation res : req.affReservation()){
                            System.out.println(res);
                        }
                    }catch(Exception e) {
                        System.out.println("Erreur dans l'affichage des reservations");
                    }
                    
                    System.out.println("Ajouter un moniteur : id idEleve idPoney idCours estPayé=>(0 ou 1)");
                    System.out.println("q/  Quitter");

                    Integer idR, idEleve, idPoney, idCours, estPayé;
                    while (true){
                        input2 = cnsl.readLine("\n Entrer votre choix : ");
                        String[] liste = null; liste = input2.split(" ");
                        if (input2.charAt(0) == 'q'){
                            break;
                        }else if (liste.length != 0){
                            try {
                                idR = Integer.parseInt(liste[0]);
                                idEleve = Integer.parseInt(liste[1]);
                                idPoney = Integer.parseInt(liste[2]);
                                idCours = Integer.parseInt(liste[3]);
                                estPayé = Integer.parseInt(liste[4]);
                                //creer une reservation avec ces infos
                                Reservation res = new Reservation(idR, idEleve, idPoney, idCours, estPayé);
                                req.setReservation(res);
                                System.out.println("reservation créé");
                                break;
                            } catch (Exception e) {
                                System.out.println("Merci de rentrer les infos séparées d'un espace ");
                            }
                        }else{
                            System.out.println("Le champs est vide");
                        }
                    }
                }else if (input.charAt(0) =='q'){ // Quitter
                    System.out.println("Bonne journée");
                    break;
                }else{
                    System.out.println("Erreur dans l'input XD");
                }
            }
        }

    }
}
