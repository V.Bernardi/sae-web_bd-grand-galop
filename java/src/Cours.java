import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Cours
 */
public class Cours {

    private int idC;
    private String date;
    private String heure;
    private int duree;
    private String activite;
    private int nbpers;
    private double prixcours;

    public Cours(int idC, String date,String heure,int duree,String activite,int nbpers,double prixcours) {
        this.idC = idC;
        this.date = date;
        this.heure = heure;
        this.duree = duree;
        this.activite = activite;
        this.nbpers = nbpers;
        this.prixcours = prixcours;
    }

    public List<Poney> getPoneys(ConnexionMySQL co) throws SQLException{
        List<Poney> liste = new ArrayList<>();
        try {
            PreparedStatement ps = co.getConnexion().prepareStatement("Select * from PONEY natural join RESERVATION where idC = ?");
            ps.setInt(1, this.idC);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("idPo");
                String nom = rs.getString("nomPo");
                int poidsaccepte = rs.getInt("poidsAccepte");
                int age = rs.getInt("agePo");
                String robe = rs.getString("robePo");
                String race = rs.getString("racePo");
                String etatsante = rs.getString("etatSante");
                int tpsAvantRepos = rs.getInt("tpsAvantRepos");
                Poney pon = new Poney(id, nom, poidsaccepte, age, robe, race, etatsante, tpsAvantRepos);
                liste.add(pon);
        }
        } catch (Exception e) {
            System.out.println("Une erreur s'est produite lors de l'obtention des poneys dans des cours");
        }

        return liste;
    }

    public List<Eleve> getEleves(ConnexionMySQL co) throws SQLException{
        List<Eleve> liste = new ArrayList<>();
        try {
            PreparedStatement ps = co.getConnexion().prepareStatement("Select * from ELEVE natural join RESERVATION where idC = ?");
            ps.setInt(1, this.idC);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("idEl");
                String nom = rs.getString("nomEl");
                String prenom = rs.getString("prenomEl");
                int poids = rs.getInt("poidsEl");
                int age = rs.getInt("ageEl");
                int niveau = rs.getInt("niveau");
                int cotisation = rs.getInt("cotisationAnn");
                Eleve el = new Eleve(id, nom, prenom, poids, age, niveau, cotisation);
                liste.add(el);
        }
        } catch (Exception e) {
            System.out.println("Une erreur s'est produite lors de l'obtention des élèves dans des cours");
        }

        return liste;
    }


    public String getActivite() {
        return activite;
    }

    public String getDate() {
        return date;
    }
    
    public int getDuree() {
        return duree;
    }

    public String getHeure() {
        return heure;
    }

    public int getidC() {
        return idC;
    }

    public int getNbpers() {
        return nbpers;
    }

    public double getPrixcours() {
        return prixcours;
    }

    public void setActivite(String activite) {
        this.activite = activite;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }
    
    public void setHeure(String heure) {
        this.heure = heure;
    }

    public void setidC(int idC) {
        this.idC = idC;
    }

    public void setNbpers(int nbpers) {
        this.nbpers = nbpers;
    }

    public void setPrixcours(double prixcours) {
        this.prixcours = prixcours;
    }    
}