public class Poney {

    private int idPo;
    private String nomPo;
    private int poidsAccepte;
    private int agePo;
    private String robePo;
    private String racePo;
    private String etatSante;
    private int tpsAvantRepos;

    public Poney(int idPo, String nompo, int poidsAccepte, int agePo, String robePo, String racePo, String etatSante, int tpsAvantRepos){
        this.idPo = idPo;
        this.nomPo = nomPo;
        this.poidsAccepte = poidsAccepte;
        this.agePo = agePo;
        this.robePo = robePo;
        this.racePo = racePo;
        this.etatSante = etatSante;
        this.tpsAvantRepos = tpsAvantRepos;
    }

    public int getAgePo() {
        return agePo;
    }

    public String getEtatSante() {
        return etatSante;
    }

    public int getIdPo() {
        return idPo;
    }

    public String getNomPo() {
        return nomPo;
    }

    public int getPoidsAccepte() {
        return poidsAccepte;
    }

    public String getRacePo() {
        return racePo;
    }

    public String getRobePo() {
        return robePo;
    }

    public int getTpsAvantRepos() {
        return tpsAvantRepos;
    }

    public void setAgePo(int agePo) {
        this.agePo = agePo;
    }

    public void setEtatSante(String etatSante) {
        this.etatSante = etatSante;
    }

    public void setIdPo(int idPo) {
        this.idPo = idPo;
    }

    public void setNomPo(String nomPo) {
        this.nomPo = nomPo;
    }

    public void setPoidsAccepte(int poidsAccepte) {
        this.poidsAccepte = poidsAccepte;
    }

    public void setRacePo(String racePo) {
        this.racePo = racePo;
    }

    public void setRobePo(String robePo) {
        this.robePo = robePo;
    }

    public void setTpsAvantRepos(int tpsAvantRepos) {
        this.tpsAvantRepos = tpsAvantRepos;
    }
}
