public class Reservation {
    
    private int idR;
    private int idEl;
    private int idPo;
    private int idC;
    private boolean estPaye;
    
    public Reservation(int idR, int idEl, int idPo, int idC, int estPaye){
        this.idR = idR;
        this.idEl = idEl;
        this.idPo = idPo;
        this.idC = idC;
        if(estPaye == 0){
            this.estPaye = true;
        }
        else{
            this.estPaye = false;
        }
    }

    public int getIdC() {
        return idC;
    }

    public int getIdEl() {
        return idEl;
    }

    public int getIdPo() {
        return idPo;
    }

    public int getIdR() {
        return idR;
    }

    public boolean getestPaye(){
        return estPaye;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public void setIdEl(int idEl) {
        this.idEl = idEl;
    }

    public void setIdPo(int idPo) {
        this.idPo = idPo;
    }

    public void setIdR(int idR) {
        this.idR = idR;
    }

    public void setEstPaye(boolean estPaye){
        this.estPaye = estPaye;
    }

}
