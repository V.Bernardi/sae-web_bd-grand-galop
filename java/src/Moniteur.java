public class Moniteur{

    private int idMo;
    private String nomMo;
    private String prenomMo;
    private double poidsMo;
    private int ageMo;
    private int niveau;
    private double salaire;
    
    public Moniteur (int idMo,String nomMo,String prenomMo,double poidsMo,int ageMo,int niveau,double salaire){
        this.idMo = idMo;
        this.nomMo = nomMo;
        this.prenomMo = prenomMo;
        this.poidsMo = poidsMo;
        this.ageMo = ageMo;
        this.niveau = niveau;
        this.salaire = salaire;

    }

    public int getAgeMo() {
        return ageMo;
    }

    public double getSalaire() {
        return salaire;
    }

    public int getIdMo() {
        return idMo;
    }

    public int getNiveau() {
        return niveau;
    }

    public String getNomMo() {
        return nomMo;
    }

    public double getPoidsMo() {
        return poidsMo;
    }

    public String getPrenomMo() {
        return prenomMo;
    }

    public void setAgeMo(int ageMo) {
        this.ageMo = ageMo;
    }

    public void setCotisationAnn(double salaire) {
        this.salaire = salaire;
    }

    public void setIdMo(int idMo) {
        this.idMo = idMo;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public void setNomMo(String nomMo) {
        this.nomMo = nomMo;
    }

    public void setPoidsMo(double poidsMo) {
        this.poidsMo = poidsMo;
    }

    public void setPrenomMo(String prenomMo) {
        this.prenomMo = prenomMo;
    }
}