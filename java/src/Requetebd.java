import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;

public class Requetebd {

    private ConnexionMySQL co;

    public Requetebd(ConnexionMySQL co){
        this.co = co;
    }

    public void setPoney(Poney poney) throws SQLException{
        try {
            PreparedStatement ps = this.co.getConnexion().prepareStatement("insert into PONEY(idPo,nomPo,poidsAccepte,agePo,robePo,racePo,etatSante,tpsAvantRepos) values(?,?,?,?,?,?,?,?)");
            ps.setInt(1, poney.getIdPo());
            ps.setString(2,poney.getNomPo());
            ps.setInt(3, poney.getPoidsAccepte());
            ps.setInt(4, poney.getAgePo());
            ps.setString(5, poney.getRobePo());
            ps.setString(6, poney.getRacePo());
            ps.setString(7, poney.getEtatSante());
            ps.setInt(8, poney.getTpsAvantRepos());
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("L'ajout du Poney n'a pas été effectué, les valeurs ne sont peut être pas bonne");
        } 
    }

    
    public void setMoniteur(Moniteur moniteur) throws SQLException{
        try {
            PreparedStatement ps = this.co.getConnexion().prepareStatement("insert into MONITEUR(idMo,nomMo,prenomMo,poidsMo,ageMo,niveau,salaire) values(?,?,?,?,?,?,?)"); 
            ps.setInt(1, moniteur.getIdMo());
            ps.setString(2,moniteur.getNomMo());
            ps.setString(3, moniteur.getPrenomMo());
            ps.setDouble(4, moniteur.getPoidsMo());
            ps.setInt(5, moniteur.getAgeMo());
            ps.setInt(6, moniteur.getNiveau());
            ps.setDouble(7, moniteur.getSalaire());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("L'ajout du Moniteur n'a pas été effectué, les valeurs ne sont peut être pas bonne");
        }

    }

    public void setEleve(Eleve eleve) throws SQLException{
        try {
            PreparedStatement ps = this.co.getConnexion().prepareStatement("insert into ELEVE(idEl,nomEl,prenomEl,poidsEl,ageEl,niveau,cotisationAnn) values(?,?,?,?,?,?,?)"); 
            ps.setInt(1, eleve.getIdEl());
            ps.setString(2,eleve.getNomEl());
            ps.setString(3, eleve.getPrenomEl());
            ps.setDouble(4, eleve.getPoidsEl());
            ps.setInt(5, eleve.getAgeEl());
            ps.setInt(6, eleve.getNiveau());
            int cotisation = 0;
            if (eleve.getCotisationAnn() == true) {
                cotisation = 0;
            } else {
                cotisation = 1;
            }
            ps.setInt(7, cotisation);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("L'ajout de l'élève n'a pas été effectué, les valeurs ne sont peut être pas bonne");
        }

    }

    public void setReservation(Reservation reservation) throws SQLException{
        try {
            PreparedStatement ps = this.co.getConnexion().prepareStatement("insert into RESERVATION(idR,idEl,idMo,idPo,idC,typeCours,estPaye) values(?,?,?,?,?,?,?)"); 
            ps.setInt(1, reservation.getIdR());
            ps.setInt(2,reservation.getIdEl());
            ps.setDouble(4, reservation.getIdPo());
            ps.setInt(5, reservation.getIdC());
            int paye = 0;
            if (reservation.getestPaye() == true) {
                paye = 0;
            } else {
                paye = 1;
            }
            ps.setInt(7, paye);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("L'ajout de la reservation n'a pas été effectué, les valeurs ne sont peut être pas bonne");
        }

    }

    public void setCours(Cours cours) throws SQLException{
        try {
            PreparedStatement ps = this.co.getConnexion().prepareStatement("insert into COURS(idC,date,heure,duree,activite,nbpers,prixcours) values(?,?,?,?,?,?,?)"); 
            ps.setInt(1, cours.getidC());
            ps.setString(2,cours.getDate());
            ps.setString(3, cours.getHeure());
            ps.setInt(4, cours.getDuree());
            ps.setString(5, cours.getActivite());
            ps.setInt(6, cours.getNbpers());
            ps.setDouble(7, cours.getPrixcours());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("L'ajout du cours n'a pas été effectué, les valeurs ne sont peut être pas bonne");
        }

    }

    public List<Poney> affPoneys() throws SQLException{
        List<Poney> poneys = new ArrayList<Poney>();
        try {
            Statement st = co.getConnexion().createStatement();
            ResultSet rs = st.executeQuery("Select * from PONEY");
            while (rs.next()) {
                int id = rs.getInt("idPo");
                String nom = rs.getString("nomPo");
                int poidsaccepte = rs.getInt("poidsAccepte");
                int age = rs.getInt("agePo");
                String robe = rs.getString("robePo");
                String race = rs.getString("racePo");
                String etatsante = rs.getString("etatSante");
                int tpsAvantRepos = rs.getInt("tpsAvantRepos");
                Poney pon = new Poney(id, nom, poidsaccepte, age, robe, race, etatsante, tpsAvantRepos);
                poneys.add(pon);
        }

        } catch (Exception e) {
            System.out.println("La récupération des poneys a rencontré une erreur");
        }
        return poneys;
    }

    public List<Eleve> affEleves() throws SQLException{
        List<Eleve> eleves = new ArrayList<Eleve>();
        try {
            Statement st = co.getConnexion().createStatement();
            ResultSet rs = st.executeQuery("Select * from ELEVE");
            while (rs.next()) {
                int id = rs.getInt("idEl");
                String nom = rs.getString("nomEl");
                String prenom = rs.getString("prenomEl");
                int poids = rs.getInt("poidsEl");
                int age = rs.getInt("ageEl");
                int niveau = rs.getInt("niveau");
                int cotisation = rs.getInt("cotisationAnn");
                Eleve el = new Eleve(id, nom, prenom, poids, age, niveau, cotisation);
                eleves.add(el);
        }

        } catch (Exception e) {
            System.out.println("La récupération des élèves a rencontré une erreur");
        }
        return eleves;
    }

    public List<Moniteur> affMoniteur() throws SQLException{
        List<Moniteur> moniteurs = new ArrayList<Moniteur>();
        try {
            Statement st = co.getConnexion().createStatement();
            ResultSet rs = st.executeQuery("Select * from MONITEUR");
            while (rs.next()) {
                int id = rs.getInt("idMo");
                String nom = rs.getString("nomMo");
                String prenom = rs.getString("prenomMo");
                int poids = rs.getInt("poidsMo");
                int age = rs.getInt("ageMo");
                int niveau = rs.getInt("niveau");
                int cotisation = rs.getInt("salaire");
                Moniteur mo = new Moniteur(id, nom, prenom, poids, age, niveau, cotisation);
                moniteurs.add(mo);
        }

        } catch (Exception e) {
            System.out.println("La récupération des moniteurs a rencontré une erreur");
        }
        return moniteurs;
    }

    public List<Reservation> affReservation() throws SQLException{
        List<Reservation> reservations = new ArrayList<Reservation>();
        try {
            Statement st = co.getConnexion().createStatement();
            ResultSet rs = st.executeQuery("Select * from RESERVATION");
            while (rs.next()) {
                int id = rs.getInt("idR");
                int eleve = rs.getInt("idEl");
                int poney = rs.getInt("idPo");
                int cours = rs.getInt("idC");
                int paye = rs.getInt("estPaye");
                Reservation res = new Reservation(id, eleve, poney, cours, paye);
                reservations.add(res);
        }

        } catch (Exception e) {
            System.out.println("La récupération des reservations a rencontré une erreur");
        }
        return reservations;
    }

    public List<Cours> affCoursEleve(Eleve eleve,Boolean quepaspaye) throws SQLException{
        List<Cours> cours = new ArrayList<Cours>();
        ResultSet rs;
        try {
            Statement st = co.getConnexion().createStatement();
            if (!quepaspaye){
                rs = st.executeQuery("Select * from COURS");
            }
            else{
                rs = st.executeQuery("select * from COURS natural join RESERVATION where estPaye = 1");
            }
            while (rs.next()) {
                int id = rs.getInt("idC");
                String date = rs.getString("date");
                String heure = rs.getString("heure");
                int duree = rs.getInt("duree");
                String activite = rs.getString("activite");
                int nbpers = rs.getInt("nbpers");
                Double prixcours = rs.getDouble("prixcours");
                Cours c = new Cours(id, date, heure, duree, activite, nbpers, prixcours);
                if (eleve.getIdEl() == id) {
                    cours.add(c);
                }
            }
        } catch (Exception e) {
            System.out.println("La récupération des cours de l'élève a rencontré une erreur");
        }
        return cours;
    }

    public List<Cours> affCoursMoniteur(Moniteur moniteur) throws SQLException{
        List<Cours> cours = new ArrayList<Cours>();
        ResultSet rs;
        try {
            Statement st = co.getConnexion().createStatement();
            rs = st.executeQuery("Select * from COURS");
            while (rs.next()) {
                int id = rs.getInt("idC");
                String date = rs.getString("date");
                String heure = rs.getString("heure");
                int duree = rs.getInt("duree");
                String activite = rs.getString("activite");
                int nbpers = rs.getInt("nbpers");
                Double prixcours = rs.getDouble("prixcours");
                Cours c = new Cours(id, date, heure, duree, activite, nbpers, prixcours);
                if (moniteur.getIdMo() == id) {
                    cours.add(c);
                }
            }
        } catch (Exception e) {
            System.out.println("La récupération des cours du moniteur a rencontré une erreur");
        }
        return cours;
    }

    public Eleve getEleve(int id) throws SQLException{
        Eleve el = new Eleve(id, "nom", "prenom", id, id, id, id);
        try {
            PreparedStatement st = co.getConnexion().prepareStatement("Select * from ELEVE where idEl = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int idel = rs.getInt("idEl");
                String nom = rs.getString("nomEl");
                String prenom = rs.getString("prenomEl");
                int poids = rs.getInt("poidsEl");
                int age = rs.getInt("ageEl");
                int niveau = rs.getInt("niveau");
                int cotisation = rs.getInt("cotisationAnn");
                el = new Eleve(idel, nom, prenom, poids, age, niveau, cotisation);
            }
        } catch (Exception e) {
            System.out.println("La récupération de l'élève a rencontré une erreur");
        }
        return el;
    } 

    public Moniteur getMoniteur(int id) throws SQLException{
        Moniteur mo = new Moniteur(id, "nom", "prenom", id, id, id, id);
        try {
            PreparedStatement st = co.getConnexion().prepareStatement("Select * from MONITEUR where idEl = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int idmo = rs.getInt("idMo");
                String nom = rs.getString("nomMo");
                String prenom = rs.getString("prenomMo");
                int poids = rs.getInt("poidsMo");
                int age = rs.getInt("ageMo");
                int niveau = rs.getInt("niveau");
                int cotisation = rs.getInt("salaire");
                mo = new Moniteur(idmo, nom, prenom, poids, age, niveau, cotisation);
            }
        } catch (Exception e) {
            System.out.println("La récupération du moniteur a rencontré une erreur");
        }
        return mo;
    } 

    public List<Integer> getListeIdCours(List<Cours> cours){
        List<Integer> liste = new ArrayList<>();
        for (Cours c : cours) {
            liste.add(c.getidC());
        }
        return liste;
    }

    public String getRole(int id){
        try{
            PreparedStatement ps = this.co.getConnexion().prepareStatement("select * from ELEVE where idEl = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                return "Eleve";
            }
            PreparedStatement ps2 = this.co.getConnexion().prepareStatement("select * from MONITEUR where idMo = ?");
            ps.setInt(1, id);
            ResultSet rs2 = ps2.executeQuery();
            while(rs2.next()){
                return "Moniteur";
            }
        }catch (Exception e) {
            System.out.println("Erreur lors de la recherche de role");
        }
        return "false";
    }


}