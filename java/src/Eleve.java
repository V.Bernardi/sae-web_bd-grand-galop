import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Eleve{

    private int idEl;
    private String nomEl;
    private String prenomEl;
    private double poidsEl;
    private int ageEl;
    private int niveau;
    private boolean cotisationAnn;

    public Eleve(int idEl,String nomEl,String prenomEl,double poidsEl,int ageEl, int niveau, int cotisationAnn){
        this.niveau = niveau;
        if (cotisationAnn == 0){
            this.cotisationAnn = true;
        }else{
            this.cotisationAnn = false;
        }
        this.idEl = idEl;
        this.nomEl = nomEl;
        this.prenomEl = prenomEl;
        this.poidsEl = poidsEl;
        this.ageEl = ageEl;
        this.niveau = niveau;
    }

    public void payerCours(int idCours, ConnexionMySQL co) throws SQLException{
        try {
            PreparedStatement ps = co.getConnexion().prepareStatement("update RESERVATION set estPaye = 0 where idC = ?"); 
            ps.setInt(1, idCours);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("L'update de payer un cours a rencontré une erreur");
        }

    }

    public void payerCotisation(){
        this.cotisationAnn = true;
    }
    
    public int getAgeEl() {
        return ageEl;
    }
    
    public int getIdEl() {
        return idEl;
    }

    public int getNiveau() {
        return niveau;
    }

    public String getNomEl() {
        return nomEl;
    }

    public double getPoidsEl() {
        return poidsEl;
    }

    public String getPrenomEl() {
        return prenomEl;
    }

    public boolean getCotisationAnn(){
        return cotisationAnn;
    }

    public void setAgeEl(int ageEl) {
        this.ageEl = ageEl;
    }

    public void getCotisationAnn(boolean cotisationAnn) {
        this.cotisationAnn = cotisationAnn;
    }

    public void setIdEl(int idEl) {
        this.idEl = idEl;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public void setNomEl(String nomEl) {
        this.nomEl = nomEl;
    }

    public void setPoidsEl(double poidsEl) {
        this.poidsEl = poidsEl;
    }

    public void setPrenomEl(String prenomEl) {
        this.prenomEl = prenomEl;
    }

    public void setCotisationAnn(boolean cotisationAnn){
        this.cotisationAnn = cotisationAnn;
    }
    
    public void reserverCours(int idCours, int idPoney, ConnexionMySQL co) throws SQLException{
        try{
            PreparedStatement ps = co.getConnexion().prepareStatement("insert into RESERVATION values(1000,?,?,1)");
            ps.setInt(2, this.idEl);
            ps.setInt(3, idPoney);
            ps.setInt(4, idCours);
            ps.executeUpdate();
        }catch (Exception e) {
            System.out.println("Erreur lors de reservation d'un cours d'un eleve");
        }
    }
}